import unittest
import sys
import datetime
import time
import logging
import json
import g10n_client

logging.basicConfig(level=logging.DEBUG)

user = None

class TestGamificationClient(unittest.TestCase):
  def test_transact(self):
    client = g10n_client.GamificationClient.get_instance()
    client.ecoins_transact(user, "test_origin", "test description", 1.5)
    self.assertTrue(True)

  def test_balance(self):
    client = g10n_client.GamificationClient.get_instance()
    balance = client.get_ecoins_user_balace(user)
    logging.info("balance: " + str(balance))
    self.assertTrue(balance)

  def test_transactions(self):
    client = g10n_client.GamificationClient.get_instance()
    transactions = client.get_ecoins_user_transactions(user)
    logging.info("transactions: " + str(transactions))
    self.assertTrue(balance)

if __name__ == '__main__':
  if len(sys.argv) < 2:
    print "usage: " + sys.argv[0] + " user_id"
    exit()
  user = sys.argv[1]
  sys.argv = sys.argv[:1]
  unittest.main()
