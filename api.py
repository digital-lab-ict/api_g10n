import io
import os
import json
import logging
import urllib2
import time

from flask import Flask, request
from flask_cors import CORS

from jose import jwt

import g10n_client

logging.basicConfig(level=logging.DEBUG)

app = Flask(__name__)
CORS(app)

DATE_FORMAT = "%Y%m%d"

def decode_token(token):
    return jwt.get_unverified_claims(token)

def get_user_id(token_payload):
    return token_payload.get("user_id").upper()

@app.route("/")
def hello():
    return "api server v1.0"

@app.route("/g10n/1.0/ecoins/report/user/balance", methods=['GET'])
def report_ecoins_user_balance():
    logging.info("report_ecoins_user_balance.1")
    ret_val = {"status": "ko", 'error': 'invalid_request'}
    try:
        user_id = get_user_id(decode_token(request.headers.get('Authorization')))
        g10n_cli = g10n_client.GamificationClient.get_instance()
        balance = g10n_cli.get_ecoins_user_balace(user_id)
        ret_val = {"status": "ok", 'data': balance}
    except Exception as e:
        logging.info("Exception: " + str(e))
    return json.dumps(ret_val)

@app.route("/g10n/1.0/ecoins/report/user/transactions", methods=['GET'])
def report_ecoins_user_transactions():
    logging.info("report_ecoins_user_transactions.1")
    ret_val = {"status": "ko", 'error': 'invalid_request'}
    try:
        g10n_cli = g10n_client.GamificationClient.get_instance()

        user_id = get_user_id(decode_token(request.headers.get('Authorization')))
        origin = request.args.get('origin')
        from_date = datetime.strptime(request.args.get('from_date'), DATE_FORMAT) if request.args.get('from_date') else None
        to_date = datetime.strptime(request.args.get('to_date'), DATE_FORMAT) if request.args.get('to_date') else None
        balance = g10n_cli.get_ecoins_user_transactions(user_id, origin, from_date, to_date)
        ret_val = {"status": "ok", 'data': balance}
    except Exception as e:
        logging.info("Exception: " + str(e))
    return json.dumps(ret_val)

@app.route("/g10n/1.0/ecoins/mgmt/user/<user_id>", methods=['POST'])
def ecoins_transact_user(user_id):
    logging.info("ecoins_transact_user.1")
    ret_val = {"status": "ko", 'message': 'invalid_request'}
    try:
        input = request.get_data()
        data = json.loads(input)
        origin = data.get("origin")
        desc = data.get("desc")
        amount = data.get("amount")
        g10n_cli = g10n_client.GamificationClient.get_instance()
        g10n_cli.ecoins_transact(user_id.upper(), origin, desc, amount)
        ret_val = {"status": "ok"}
    except Exception as e:
        logging.info("Exception: " + str(e))
    return json.dumps(ret_val)


@app.route("/swagger")
def test():
  return app.send_static_file('api_g10n_swagger.yaml')

if __name__ == "__main__":
    app.run(host='0.0.0.0',port=5000,threaded=True)
