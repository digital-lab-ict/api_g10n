import os
from cassandra import cluster
from cassandra import auth
from cassandra.query import dict_factory
from cassandra.cqlengine import models
from cassandra.cqlengine import columns
from cassandra.cqlengine import connection
from cassandra.cqlengine import query
from cassandra.cqlengine import management

import decimal
import datetime
import time
import sets
import logging

DATE_FORMAT = "%Y%m%d"

class GamificationClient:
    KEYSPACE_NAME = "g10n"

    instance = None

    @classmethod
    def get_instance(cls):
        instance = cls.instance
        if instance is None:
            instance = GamificationClient()
            cls.instance = instance
        return instance

    def __init__(self):
        cassandra_hosts = os.getenv('G10N_DB_HOSTS').split(',')
        cassandra_user = os.getenv('G10N_DB_USER')
        cassandra_password = os.getenv('G10N_DB_PASSWORD')
        logging.info("hosts: " +str(cassandra_hosts))

        auth_provider = auth.PlainTextAuthProvider(username=cassandra_user, password=cassandra_password)
        self.cluster = cluster.Cluster(cassandra_hosts, auth_provider=auth_provider)
        self.session = self.cluster.connect()
        self.session.row_factory = dict_factory
        connection.set_session(self.session)
        self.session.set_keyspace(self.KEYSPACE_NAME)
        self.UserCoinsBalance.__keyspace__=self.KEYSPACE_NAME
        self.UserCoinsTransaction.__keyspace__=self.KEYSPACE_NAME
        management.sync_table(self.UserCoinsBalance)
        management.sync_table(self.UserCoinsTransaction)

    def ecoins_transact(self, user_id, origin, desc, amount):
        try:
            transaction = self.UserCoinsTransaction(user_id=user_id, ts=datetime.datetime.now())
            transaction.origin = origin
            transaction.desc = desc
            transaction.amount = decimal.Decimal(amount)
            transaction.save()

            ucb = None
            try:
                ucb = self.UserCoinsBalance.get(user_id=user_id)
            except query.DoesNotExist as e:
                ucb = self.UserCoinsBalance(user_id=user_id)
                ucb.balance = decimal.Decimal(0.0)

            ucb.balance += transaction.amount
            ucb.last_update = transaction.ts
            ucb.save()
        except query.DoesNotExist as e:
            return None

    def get_ecoins_user_balace(self, user_id):
        try:
            ucb = self.UserCoinsBalance.get(user_id=user_id)
            return ucb.as_dict()
        except query.DoesNotExist as e:
            return None

    def get_ecoins_user_transactions(self, user_id, origin=None, from_date=None, to_date=None):
        try:
            q = self.UserCoinsTransaction.objects.filter(user_id=user_id)
            q = q.filter(origin=origin) if origin else q
            q = q.from_date(ts>=from_date) if from_date else q
            q = q.to_date(ts<=to_date) if to_date else q
            transactions = []
            for t in q.all():
                transactions.append(t.as_dict())
            return transactions
        except query.DoesNotExist as e:
            return None

    class UserCoinsBalance(models.Model):
        user_id = columns.Text(primary_key=True)
        balance = columns.Decimal()
        last_update = columns.DateTime()

        def as_dict(self):
            return {"user_id": self.user_id, "balance": float(self.balance), "last_update": str(self.last_update)}

    class UserCoinsTransaction(models.Model):
        user_id = columns.Text(primary_key=True)
        origin = columns.Text()
        desc = columns.Text()
        amount = columns.Decimal()
        ts = columns.DateTime(primary_key=True)
        def as_dict(self):
            return {"user_id": self.user_id, "origin": self.origin, "desc": self.desc, "amount": float(self.amount), "timestamp": str(self.ts)}
